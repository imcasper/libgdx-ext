import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.artifacts.repositories.PasswordCredentials
import org.gradle.jvm.toolchain.JavaLanguageVersion

plugins {
    id 'org.jetbrains.kotlin.plugin.serialization'
    id 'org.jetbrains.kotlin.jvm'
    id 'java-library'
    id 'maven-publish'
    id 'signing'
}

group = 'io.gitlab.casperix'
version = '1.2.1'

repositories {
    mavenCentral()
    mavenLocal()
}

java {
    withJavadocJar()
    withSourcesJar()
    toolchain {
        languageVersion = JavaLanguageVersion.of(javaVersion)
    }
}

apply plugin: "java-library"
apply plugin: "kotlinx-serialization"
apply plugin: "org.jetbrains.kotlin.jvm"

dependencies {
    api project(":gdx-types-adapter")
    api "io.gitlab.casperix:render2d-api:1.2.1"

    implementation "io.gitlab.casperix:math:1.7.0"
    implementation "io.gitlab.casperix:misc:1.7.0"

    implementation "com.badlogicgames.gdx:gdx-freetype:$gdxVersion"
    implementation "com.esotericsoftware.spine:spine-libgdx:4.0.18.1"
    testImplementation 'org.jetbrains.kotlin:kotlin-test'
    testImplementation 'org.jetbrains.kotlin:kotlin-test-junit'

    // https://mvnrepository.com/artifact/org.mockito/mockito-core
    testImplementation "org.mockito:mockito-core:5.6.0"
//    testImplementation "com.badlogicgames.gdx:gdx-backend-headless:$gdxVersion"
    testImplementation "com.badlogicgames.gdx:gdx-backend-lwjgl3:$gdxVersion"
    testImplementation "com.badlogicgames.gdx:gdx:$gdxVersion"
    testImplementation "com.badlogicgames.gdx:gdx-platform:$gdxVersion:natives-desktop"
}

artifacts {
    archives javadocJar, sourcesJar
}

signing {
    if (!version.endsWith("SNAPSHOT")) {
        sign(publishing.publications )
    }
}

publishing {
    repositories {
        maven {
            name = 'sonatype'
            credentials {
                username = ossrhUsername
                password = ossrhPassword
            }
            def releasesRepoUrl = 'https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/'
            def snapshotsRepoUrl = 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
            url = version.endsWith('SNAPSHOT') ? snapshotsRepoUrl : releasesRepoUrl
        }
    }
    publications {
        custom(MavenPublication) {
            from components.java
//
//            versionMapping {
//                usage('java-api') {
//                    fromResolutionOf('runtimeClasspath')
//                }
//                usage('java-runtime') {
//                    fromResolutionResult()
//                }
//            }
//
//            groupId = group
//            artifactId = project.name
//            version = version

            pom {
                name = project.name
                description = 'Implementation renderer2d-api on LibGDX'
                url = "https://gitlab.com/casperix/$name"
                licenses {
                    license {
                        name = 'The Apache License, Version 2.0'
                        url = 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                    }
                }
                developers {
                    developer {
                        id = 'casperix'
                        name = 'Mikhail Lobachevsky'
                        email = 'casper1bit@gmail.com'
                    }
                }
                scm {
                    connection = "scm:git:git://gitlab.com/casperix/${name}.git"
                    developerConnection = "scm:git:ssh://gitlab.com/casperix/${name}.git"
                    url = "https://gitlab.com/casperix/$name/"
                }
            }
        }
    }
}