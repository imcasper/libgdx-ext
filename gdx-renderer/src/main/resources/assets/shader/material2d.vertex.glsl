attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;
attribute vec2 a_tangent;

uniform mat4 u_projection_view;
uniform vec3 u_light_pos;

varying vec4 v_color;
varying vec2 v_tex_coords;
varying vec3 v_normal;
varying vec3 v_ts_light_pos;
varying vec3 v_ts_frag_pos;

varying vec2 v_tangent;

void main()
{
    v_tangent = a_tangent;
    vec3 T = normalize(vec3(a_tangent, 0.0));
    vec3 N = vec3(0.0, 0.0, 1.0);
    vec3 B = cross(T, N);

    mat3 TBN = transpose(mat3(T, B, N));
    v_ts_light_pos = TBN * u_light_pos;
    v_ts_frag_pos  = TBN * a_position.xyz;

    v_color = a_color;
//    v_color = a_color * 0.01 + vec4(v_ts_light_pos * 0.1, 1.0);
    v_color.a = v_color.a * (255.0/254.0);
    v_tex_coords = a_texCoord0;
    gl_Position =  u_projection_view * a_position;
}
