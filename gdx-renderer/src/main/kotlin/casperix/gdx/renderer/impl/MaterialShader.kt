package casperix.gdx.renderer.impl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.utils.GdxRuntimeException

class MaterialShader : Shader {
    val program = ShaderProgram(
        Gdx.files.classpath("assets/shader/material2d.vertex.glsl").readString(),
        Gdx.files.classpath("assets/shader/material2d.fragment.glsl").readString()
    )

    var camera: Camera? = null
    var context: RenderContext? = null

    override fun init() {
        if (!program.isCompiled) throw GdxRuntimeException(program.log)
    }

    override fun dispose() {
        program.dispose()
    }

    override fun begin(camera: Camera, context: RenderContext?) {
        this.camera = camera
        this.context = context
        program.begin()
        program.setUniformMatrix("u_projViewTrans", camera.combined)
    }

    override fun render(renderable: Renderable) {
        program.setUniformMatrix("u_worldTrans", renderable.worldTransform)
        renderable.meshPart.render(program)
    }

    override fun end() {
        program.end()
    }

    override fun compareTo(other: Shader): Int {
        return 0
    }

    override fun canRender(instance: Renderable): Boolean {
        return true
    }
}



