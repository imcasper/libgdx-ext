package casperix.gdx.renderer

import casperix.gdx.renderer.impl.GdxRenderer2D
import casperix.math.color.Colors
import casperix.math.geometry.*
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.quad_matrix.float32.Matrix4f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.float32.Vector3f
import casperix.renderer.Environment
import casperix.renderer.Renderer2D
import kotlin.test.Test


class GdxPixmapRenderTest : GdxTest() {

    @Test
    fun some() {
        renderTask = ::test
        waitFinish()
    }

    fun test() {
        try {
            drawFrame()
        } catch (e: Throwable) {
            e.printStackTrace()
        } finally {
            close()
        }
    }

    private fun drawFrame() {
        val renderer: Renderer2D = GdxRenderer2D()
        val textureA = Bitmap(RGBAMap(64, 128))
        val textureB = Bitmap(RGBAMap(128, 64))
        val colorA = Colors.BLACK.toColor4f()
        val colorB = Colors.WHITE.toColor4f()

        renderer.environment = Environment(Matrix3f.IDENTITY, Matrix3f.IDENTITY, Colors.NAVY, Vector3f.ZERO)

        val materials = listOf(
            Material(null, null, null),
            Material(colorA, null, null),
            Material(null, textureA, null),
            Material(null, null, textureB),
            Material(null, textureA, textureB),
            Material(colorA, textureA, null),
            Material(colorA, null, textureB),
            Material(colorA, textureA, textureB),
        )

        val colors = listOf(colorA, colorB)
        val textures = listOf(textureA, textureB)

        val points = listOf(Vector2f.ZERO, Vector2f.ONE)
        val lines = listOf(Line2f(Vector2f.ZERO, Vector2f.ONE))
        val triangles = listOf(Triangle2f(Vector2f(1f, 2f), Vector2f(3f, 4f), Vector2f(5f, 6f)))
        val quads = listOf(Quad2f(Vector2f(1f, 2f), Vector2f(3f, 4f), Vector2f(5f, 6f), Vector2f(0f, 1f)))
        val polygons =
            listOf<Polygon2f>(
                CustomPolygon(listOf()),
                CustomPolygon(listOf(Vector2f(1f, 2f))),
                CustomPolygon(listOf(Vector2f(1f, 2f), Vector2f(3f, 4f))),
                CustomPolygon(listOf(Vector2f(1f, 2f), Vector2f(3f, 4f), Vector2f(5f, 6f))),
                CustomPolygon(listOf(Vector2f(1f, 2f), Vector2f(3f, 4f), Vector2f(5f, 6f), Vector2f(0f, 1f))),
                CustomPolygon(
                    listOf(
                        Vector2f(1f, 2f),
                        Vector2f(3f, 4f),
                        Vector2f(5f, 6f),
                        Vector2f(0f, 1f),
                        Vector2f(-2f, -1f)
                    )
                ),
            )

        materials.forEach { material ->
            triangles.forEach { triangle ->
                renderer.drawTriangle(material, triangle)
            }

            quads.forEach { quad ->
                renderer.drawQuad(material, quad)
            }
            polygons.forEach { polygon ->
                renderer.drawPolygon(material, polygon)
            }
            points.forEach { point ->
                renderer.drawPoint(material, point, -2f)
                renderer.drawPoint(material, point, 0f)
                renderer.drawPoint(material, point, 2f)
            }
            lines.forEach { line ->
                renderer.drawLine(material, line, -2f)
                renderer.drawLine(material, line, 0f)
                renderer.drawLine(material, line, 2f)
            }
        }

        colors.forEach { color ->
            triangles.forEach { triangle ->
                renderer.drawTriangle(color, triangle)
            }

            quads.forEach { quad ->
                renderer.drawQuad(color, quad)
            }
            polygons.forEach { polygon ->
                renderer.drawPolygon(color, polygon)
            }
            points.forEach { point ->
                renderer.drawPoint(color, point, -2f)
                renderer.drawPoint(color, point, 0f)
                renderer.drawPoint(color, point, 2f)
            }
            lines.forEach { line ->
                renderer.drawLine(color, line, -2f)
                renderer.drawLine(color, line, 0f)
                renderer.drawLine(color, line, 2f)
            }
        }

        textures.forEach { texture ->
            triangles.forEach { triangle ->
                renderer.drawTriangle(texture, triangle)
            }

            quads.forEach { quad ->
                renderer.drawQuad(texture, quad)
            }
            polygons.forEach { polygon ->
                renderer.drawPolygon(texture, polygon)
            }
            points.forEach { point ->
                renderer.drawPoint(texture, point, -2f)
                renderer.drawPoint(texture, point, 0f)
                renderer.drawPoint(texture, point, 2f)
            }
            lines.forEach { line ->
                renderer.drawLine(texture, line, -2f)
                renderer.drawLine(texture, line, 0f)
                renderer.drawLine(texture, line, 2f)
            }
        }

        renderer.end()
    }


}