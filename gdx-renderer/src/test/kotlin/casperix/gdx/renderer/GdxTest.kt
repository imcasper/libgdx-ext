package casperix.gdx.renderer

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import org.junit.AfterClass
import org.junit.BeforeClass
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

abstract class GdxTest {
    // This is our "test" application
    companion object {
        var renderTask: (() -> Unit)? = null

        private var t: Thread? = null

        private var inited = AtomicBoolean(false)
        private var finished = AtomicBoolean(false)

        fun close() {
            finished.set(true)
        }

        // Before running any tests, initialize the application with the headless backend
        @JvmStatic
        @BeforeClass
        fun init(): Unit {
            // Note that we don't need to implement any of the listener's methods
            t = thread {
                Lwjgl3Application(object : ApplicationListener {
                    override fun create() {
                        inited.set(true)
                    }

                    override fun resize(width: Int, height: Int) {}
                    override fun render() {
                        val currentRenderTask = renderTask ?: return
                        currentRenderTask()
                        renderTask = null
                    }

                    override fun pause() {}
                    override fun resume() {}
                    override fun dispose() {}
                })
            }
            while (!inited.get()) {
                Thread.sleep(1L)
            }

            // Use Mockito to mock the OpenGL methods since we are running headlessly
//            Gdx.gl20 = Mockito.mock(GL20::class.java)
//            Gdx.gl = Gdx.gl20
        }


        fun waitFinish() {
            while (!finished.get()) {
                Thread.sleep(1L)
            }
        }

        // After we are done, clean up the application
        @JvmStatic
        @AfterClass
        fun cleanUp(): Unit {
            waitFinish()
            t?.interrupt()
        }
    }
}