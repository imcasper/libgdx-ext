package casperix.camera

import casperix.math.axis_aligned.dimensionOf
import casperix.math.camera.OrthographicCamera2f
import casperix.misc.Updatable
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor

class GdxOrthographicCameraController(private val camera: OrthographicCamera2f, config: CameraConfig) : Updatable,
    InputProcessor by GdxOrthographicCameraInput(config, camera) {

    init {
        update()
    }


    override fun update() {
        camera.viewport = dimensionOf(Gdx.graphics.width, Gdx.graphics.height)
    }
}