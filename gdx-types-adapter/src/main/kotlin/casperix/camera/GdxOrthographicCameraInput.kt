package casperix.camera

import casperix.math.axis_aligned.float32.Box2f
import casperix.math.camera.OrthographicCamera2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.misc.clamp
import casperix.misc.time.getTimeMs
import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.InputAdapter


class GdxOrthographicCameraInput(val config: CameraConfig, val camera: OrthographicCamera2f) :
    InputAdapter() {
    private var zoomFactor = 1.1f
    private var startPoint: Vector2i? = null
    private val translateButton = Buttons.RIGHT

    init {
        //  apply camera limits:
        setZoom(camera.zoom)
        setPosition(camera.position)
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (translateButton != button) {
            return false
        }
        startPoint = Vector2i(screenX, screenY)
        return true
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (translateButton != button) {
            return false
        }
        startPoint = null
        return true
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val lastPoint = startPoint ?: return false
        val currentPoint = Vector2i(screenX, screenY)

        val delta = (currentPoint - lastPoint) * Vector2i(1, -1)
        startPoint = currentPoint
        setPosition(camera.position - delta.toVector2f() * camera.zoom)
        return true
    }

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        if (amountY != 0f) {
            val factor = if (amountY > 0) zoomFactor
            else 1f / zoomFactor

            setZoom(camera.zoom * factor)

        }
        return false
    }

    private fun bound(value: Vector2f, bound: Box2f): Vector2f {
        val min = bound.min
        val max = bound.max
        var result = value
        if (result.x < min.x) result = result.copy(x = min.x)
        if (result.y < min.y) result = result.copy(y = min.y)
        if (result.x > max.x) result = result.copy(x = max.x)
        if (result.y > max.y) result = result.copy(y = max.y)
        return result

    }

    private fun setPosition(value: Vector2f) {
        camera.position = if (config.bound != null) {
            bound(value, config.bound)
        } else {
            value
        }
    }

    private fun setZoom(value: Float) {
        camera.zoom = value.clamp(config.minZoom, config.maxZoom)
    }
}