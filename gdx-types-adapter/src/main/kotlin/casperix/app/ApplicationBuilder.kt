package casperix.app

import com.badlogic.gdx.ApplicationAdapter

class ApplicationBuilder(val builder: () -> SafeApplicationAdapter) : ApplicationAdapter() {
    private var adapter: SafeApplicationAdapter? = null

    override fun create() {
        super.create()
        adapter?.dispose()
        adapter = builder()
    }

    override fun render() {
        super.render()
        adapter?.render()
    }

    override fun resize(width: Int, height: Int) {
        adapter?.resize(width, height)
        super.resize(width, height)
    }

    override fun dispose() {
        super.dispose()
        adapter?.dispose()
        adapter = null;
    }

}