package casperix.app

import casperix.math.axis_aligned.int32.Dimension2i
import casperix.signals.concrete.EmptyPromise
import casperix.signals.concrete.Promise

/**
 * 	Abstract proxy for application
 */
interface WindowWatcher {
    val onResize: Promise<Dimension2i>
    val onUpdate: Promise<Double>
    val onPreRender: EmptyPromise
    val onRender: EmptyPromise
    val onPostRender: EmptyPromise
    val onExit: EmptyPromise
}