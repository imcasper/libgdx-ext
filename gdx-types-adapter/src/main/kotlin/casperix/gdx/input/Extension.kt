package casperix.gdx.input

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor


fun registerInput(input: InputProcessor) {
	var inputEntry = Gdx.input.inputProcessor
	if (inputEntry !is InputMultiplexer) {
		inputEntry = if (inputEntry != null) InputMultiplexer(inputEntry) else InputMultiplexer()
		Gdx.input.inputProcessor = inputEntry
	}
	inputEntry.addProcessor( input)
}
//fun registerInput(input: InputAdapter) {
//	val inputProcessor = Gdx.input.inputProcessor
//	if (inputProcessor is InputMultiplexer) {
//		inputProcessor.addProcessor(input)
//	} else if (inputProcessor == null) {
//		Gdx.input.inputProcessor = input
//	} else {
//		Gdx.input.inputProcessor = InputMultiplexer(inputProcessor, input)
//	}
//}
fun unregisterInput(input: InputProcessor) {
	var inputEntry = Gdx.input.inputProcessor
	if (inputEntry !is InputMultiplexer) {
		inputEntry = if (inputEntry != null) InputMultiplexer(inputEntry) else InputMultiplexer()
		Gdx.input.inputProcessor = inputEntry
	}
	inputEntry.removeProcessor(input)
}

