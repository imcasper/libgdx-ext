package casperix.gdx.ui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.ui.*

object DefaultSkin {
	fun create():Skin {
		val skin = Skin()

		val defaultFont = BitmapFont()
		skin.add("default", defaultFont)

		// Generate a 1x1 white texture and store it in the skin named "white".
		val pixmap = Pixmap(10, 10, Pixmap.Format.RGBA8888)
		pixmap.setColor(Color.WHITE)
		pixmap.fill()
		skin.add("white", Texture(pixmap))


		val treeStyle = Tree.TreeStyle()
		treeStyle.background = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.minus = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.minusOver = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.plus = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.plusOver = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.over = skin.newDrawable("white", Color.DARK_GRAY)
		treeStyle.selection = skin.newDrawable("white", Color.DARK_GRAY)
		skin.add("default", treeStyle)

		// Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
		val defaultButton = TextButton.TextButtonStyle()
		defaultButton.up = skin.newDrawable("white", Color.GRAY)
		defaultButton.down =  skin.newDrawable("white", Color.LIGHT_GRAY)
		defaultButton.checked =  skin.newDrawable("white", Color.ROYAL)
		defaultButton.over = skin.newDrawable("white", Color.LIGHT_GRAY)
		defaultButton.checkedOver = skin.newDrawable("white", Color.SKY)
		defaultButton.font = defaultFont
		skin.add("default", defaultButton)

		val progressBar = ProgressBar.ProgressBarStyle()
		progressBar.background = skin.newDrawable("white", Color.DARK_GRAY)
		progressBar.knobBefore = skin.newDrawable("white", Color.CYAN)
		progressBar.knob = skin.newDrawable("white", Color.CYAN)
		progressBar.knobAfter = skin.newDrawable("white", Color.BLUE)
		skin.add("default", progressBar)

		val defaultLabel = Label.LabelStyle()
		defaultLabel.font = defaultFont
		defaultLabel.fontColor = Color.WHITE
		skin.add("default", defaultLabel)

		val defaultTextField = TextField.TextFieldStyle()
		defaultTextField.font = defaultFont
		defaultTextField.fontColor = Color.WHITE
		skin.add("default", defaultTextField)

		val toolBarStyle = ToolBar.ToolBarStyle(skin.newDrawable("white"))
		skin.add("default", toolBarStyle)

		return skin
	}
}