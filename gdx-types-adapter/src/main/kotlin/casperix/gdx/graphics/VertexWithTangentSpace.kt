package casperix.gdx.graphics

import casperix.math.color.float32.Color4f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.float32.Vector3f

data class VertexWithTangentSpace(
    val position: Vector3f,
    val texCoord: Vector2f,
    val color: Color4f,
    val normal: Vector3f,
    val binormal: Vector3f,
    val tangent: Vector3f
)