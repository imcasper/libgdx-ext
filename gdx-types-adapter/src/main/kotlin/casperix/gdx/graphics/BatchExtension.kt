package casperix.gdx.graphics

import casperix.math.geometry.Line2f
import casperix.math.geometry.Quad2f
import casperix.math.geometry.Triangle2f
import casperix.math.vector.float32.Vector2f
import com.badlogic.gdx.graphics.glutils.ShapeRenderer


fun ShapeRenderer.point(value: Vector2f) {
    point(value.x, value.y, 0f)
}

fun ShapeRenderer.line(value: Line2f) {
    value.apply {
        line(v0, v1)
    }
}

fun ShapeRenderer.line(A: Vector2f, B: Vector2f) {
    line(A.x, A.y, 0f, B.x, B.y, 0f)
}

fun ShapeRenderer.quad(quad: Quad2f) {
    triangle(quad.getFace(0))
    triangle(quad.getFace(1))
}

fun ShapeRenderer.triangle(triangle: Triangle2f) {
    triangle(triangle.v0.x, triangle.v0.y, triangle.v1.x, triangle.v1.y, triangle.v2.x, triangle.v2.y)
}

//fun ShapeRenderer.polygon(value: Polygon2f) {
//    val triangleAmount = value.vertices.size - 2
//    (0 until triangleAmount).forEach { triangleId ->
//        val vA = value.vertices[0]
//        val vB = value.vertices[triangleId + 1]
//        val vC = value.vertices[triangleId + 2]
//        triangle(vA, vB, vC)
//    }
//}

fun ShapeRenderer.triangle(a: Vector2f, b: Vector2f, c: Vector2f) {
    triangle(
        a.x, a.y,
        b.x, b.y,
        c.x, c.y
    )
}

//private fun Array<Vector3D>.asFloatArrayVertices(): FloatArray {
//    return FloatArray(size * 3) { index ->
//        val globalOffset = index / 3
//        val localOffset = index % 3
//        val point = get(globalOffset)
//        when (localOffset) {
//            0 -> point.x
//            1 -> point.y
//            2 -> point.z
//            else -> throw Error("Invalid offset: $localOffset")
//        }.toFloat()
//    }
//}
//
//private fun Array<Vector2D>.asFloatArrayVertices(): FloatArray {
//    return FloatArray(size * 3) { index ->
//        val globalOffset = index / 3
//        val localOffset = index % 3
//        val point = get(globalOffset)
//        when (localOffset) {
//            0 -> point.x
//            1 -> point.y
//            2 -> 0f
//            else -> throw Error("Invalid offset: $localOffset")
//        }.toFloat()
//    }
//}
