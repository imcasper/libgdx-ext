package casperix.gdx.graphics

import casperix.math.vector.float64.Vector2d
import casperix.math.vector.float64.Vector3d
import casperix.math.vector.int32.Vector2i
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.ConeShapeBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.CylinderShapeBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.SphereShapeBuilder
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import kotlin.math.PI

/**
 * 	Simplest creation of test models
 */
object ModelBuilder {

	fun sphere(material: Material = Material(), vertexAttributesMask: Int = GeometryBuilder.defaultVertexAttributes, size: Vector3d = Vector3d.ZERO, divisions: Vector2i = Vector2i(10, 10), uRange: Vector2d = Vector2d(0.0, 360.0), vRange: Vector2d = Vector2d(0.0, 180.0)): Model {
		return GeometryBuilder.run(material = material, vertexAttributesMask = vertexAttributesMask) {
			SphereShapeBuilder.build(it, size.x.toFloat(), size.y.toFloat(), size.z.toFloat(), divisions.x, divisions.y, uRange.x.toFloat(), uRange.y.toFloat(), vRange.x.toFloat(), vRange.y.toFloat())
		}
	}

	fun cone(material: Material = Material(), vertexAttributesMask: Int = GeometryBuilder.defaultVertexAttributes, size: Vector3d = Vector3d.ZERO, divisions: Int = 10, range: Vector2d = Vector2d(0.0, 360.0), zUp:Boolean = true): Model {
		return GeometryBuilder.run(material = material, vertexAttributesMask = vertexAttributesMask) {
			if (zUp) transformForZ(it)
			ConeShapeBuilder.build(it, size.x.toFloat(), size.y.toFloat(), size.z.toFloat(), divisions, range.x.toFloat(), range.y.toFloat())
		}
	}

	fun cube(material: Material = Material(), vertexAttributesMask: Int = GeometryBuilder.defaultVertexAttributes, size: Vector3d = Vector3d.ZERO): Model {
		return GeometryBuilder.run(material = material, vertexAttributesMask = vertexAttributesMask) {
			BoxShapeBuilder.build(it, size.x.toFloat(), size.y.toFloat(), size.z.toFloat())
		}
	}

	fun cylinder(material: Material = Material(), vertexAttributesMask: Int = GeometryBuilder.defaultVertexAttributes, size: Vector3d = Vector3d.ZERO, divisions: Int = 10, range: Vector2d = Vector2d(0.0, 360.0), zUp:Boolean = true): Model {
		return GeometryBuilder.run(material = material, vertexAttributesMask = vertexAttributesMask) {
			if (zUp) transformForZ(it)
			CylinderShapeBuilder.build(it, size.x.toFloat(), size.y.toFloat(), size.z.toFloat(), divisions, range.x.toFloat(), range.y.toFloat())
		}
	}

	private fun transformForZ(it:MeshPartBuilder) {
		val matrix = Matrix4()
		matrix.setToRotationRad(Vector3.X, PI.toFloat() / 2f)
		it.setVertexTransform(matrix)
	}
}