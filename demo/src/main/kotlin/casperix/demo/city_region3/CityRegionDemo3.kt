package casperix.demo.city_region3

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.shape_builder.PolygonEditor
import casperix.math.mesh.RTreeMeshBuilder
import casperix.math.mesh.float32.Mesh
import casperix.math.random.nextPolygon2f
import casperix.math.random.nextQuad2f
import casperix.math.random.nextTriangle2f
import casperix.math.vector.float32.Vector2f
import casperix.mesh.city.City
import casperix.mesh.city.component.CityRegionBuilder
import casperix.mesh.city.component.CityRegionConfig
import casperix.mesh.city.render.CityMeshRender
import casperix.renderer.Renderer2D
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.measureTime


class CityRegionDemo3(val random: Random) : AbstractDemo() {
    private val shapeEditor = PolygonEditor(pointManager, random.nextQuad2f(minOffset = Vector2f(-10f), maxOffset = Vector2f(10f)))

    private var input = Mesh()
    private var time = Duration.ZERO
    private var xSkip = 1

    init {
        actionManager += TitleEntry("shape")
        actionManager += SimpleAction("triangle") { shapeEditor.setShape(random.nextTriangle2f(maxOffset = Vector2f(10f)));rebuild() }
        actionManager += SimpleAction("quad") { shapeEditor.setShape(random.nextQuad2f(maxOffset = Vector2f(10f)));rebuild() }
        actionManager += SimpleAction("polygon") { shapeEditor.setShape(random.nextPolygon2f(maxOffset = Vector2f(10f)));rebuild() }
        actionManager += TitleEntry("x-skip")
        actionManager += SelectorAction("x-skip", "1") { xSkip = 1;rebuild() }
        actionManager += SelectorAction("x-skip", "2") { xSkip = 2;rebuild() }
        actionManager += SelectorAction("x-skip", "3") { xSkip = 3;rebuild() }
        pointManager.pointDragged.then { rebuild() }

        rebuild()
    }

    override fun getArticles(): List<String> {
        return listOf(
            "points: " + input.points.size,
            "edges: " + input.edges.size,
            "regions: " + input.regions.size,
            "time:  " + time.inWholeMilliseconds + "ms",
        )
    }

    override fun update(customPoint: Vector2f) {

    }

    private fun rebuild() {
        time = measureTime {
            input = CityRegionBuilder.build(RTreeMeshBuilder(), City(), shapeEditor.shape, 128f)
        }
    }

    override fun renderScene(renderer: Renderer2D) {
        CityMeshRender.render(renderer, input, true, true, true)
    }

}

