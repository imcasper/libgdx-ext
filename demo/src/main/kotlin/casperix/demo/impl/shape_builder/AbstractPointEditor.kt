package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.misc.Disposable

abstract class AbstractPointEditor(val pointManager: ControlPointManager) : Disposable {
    val selfPoints = mutableListOf<ControlPoint>()

    init {
        pointManager.groups += selfPoints
    }

    override fun dispose() {
        pointManager.groups -= selfPoints
    }

    fun getPoints(): List<ControlPoint> {
        return selfPoints
    }
}