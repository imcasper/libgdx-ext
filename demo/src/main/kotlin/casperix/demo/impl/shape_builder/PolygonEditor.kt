package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.geometry.CustomPolygon
import casperix.math.geometry.Polygon2f

class PolygonEditor(
    pointManager: ControlPointManager,
    initial: Polygon2f,
) : AbstractPointEditor(pointManager), ShapeEditor {

    override val shape: Polygon2f get() = CustomPolygon(selfPoints.map { it.position })

    init {
        setShape(initial)
    }

    fun setShape(initial: Polygon2f) {
        selfPoints.clear()
        selfPoints += initial.getVertices().map {
            ControlPoint(it)
        }
    }

}