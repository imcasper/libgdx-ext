package casperix.demo.impl

import casperix.demo.impl.action.*
import casperix.demo.impl.control_point.ControlPointManager
import casperix.demo.impl.shape_builder.CurveBuilder
import casperix.demo.misc.Assets
import casperix.demo.misc.CameraProvider
import casperix.demo.util.RenderConfig
import casperix.demo.util.RenderConfig.lineThick
import casperix.demo.util.RenderConfig.pointDiameter
import casperix.demo.util.UI.addRow
import casperix.demo.util.UI.createButton
import casperix.demo.util.UI.createToggleButton
import casperix.gdx.ui.ToggleGroup
import casperix.gdx.ui.simulateClick
import casperix.input.InputEvent
import casperix.math.color.Color
import casperix.math.color.Colors.GREEN
import casperix.math.color.Colors.RED
import casperix.math.curve.float32.Curve2f
import casperix.math.geometry.Line2f
import casperix.math.geometry.Polygon2f
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.misc.ceilToInt
import casperix.misc.clamp
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton

abstract class AbstractDemo : Demo {
    protected val baseStep = 0.5f
    protected val limitLength = 10_000
    protected var showNormals = false
    protected var showHelpers = false

    val pointManager = ControlPointManager()
    val actionManager = GuiManager()


    init {
        actionManager += SwitchAction("show normals") { showNormals = it }
        actionManager += SwitchAction("show helpers") { showHelpers = it }
        actionManager += TitleEntry("gird align")
        actionManager += SelectorAction("gird", "off") { pointManager.girdAlignStep = null }
        actionManager += SelectorAction("gird", "1.0") { pointManager.girdAlignStep = 1f }
        actionManager += SelectorAction("gird", "0.2") { pointManager.girdAlignStep = 0.2f }
        actionManager += DummyEntry
    }

    fun register(builder: CurveBuilder) {
        pointManager.groups += builder.getPoints()
    }

    open fun getArticles(): List<String> {
        return emptyList()
    }


    private fun getSelfArticles(): List<String> {
        val camera = CameraProvider.camera
        val zoom = camera.zoom.toPrecision(5)

        val selected = pointManager.selected
        val moveInfo = if (selected != null) {
            "move: " + selected.position.toPrecision(4)
        } else {
            "try drag points"
        }

        return listOf(moveInfo, "zoom: $zoom")
    }

    override fun getDetail(): String {
        return (getSelfArticles() + getArticles()).joinToString("\n")
    }

    fun Renderer2D.drawLine(color: Color, line: Line2f) {
        drawLine(color, line, lineThick)
        drawPoint(color, line.v0, pointDiameter)
        drawPoint(color, line.v1, pointDiameter)
    }

    fun Renderer2D.drawPolygonWithContour(color: Color, shape: Polygon2f) {
        drawPolygonWithContour(color, RenderConfig.SHAPE_SECONDARY, shape, lineThick)
    }

    fun Renderer2D.drawSegment(color: Color, segment: LineSegment2f) {
        drawSegment(color, segment, lineThick)
        drawPoint(color, segment.start, pointDiameter)
        drawPoint(color, segment.finish, pointDiameter)
    }

    fun Renderer2D.drawPoint(color: Color, value: Vector2f) {
        drawPoint(color, value, pointDiameter)
    }

    fun Renderer2D.drawPoint(color: Color, value: Vector2i) {
        drawPoint(color, value.toVector2f(), pointDiameter)
    }

    override fun input(event: InputEvent) {
        pointManager.input(event)
    }

    final override fun render(renderer: Renderer2D) {
        BasisRender.render(renderer)
        renderScene(renderer)
        pointManager.render(renderer)
    }

    abstract fun renderScene(renderer: Renderer2D)


    fun Renderer2D.drawCurve(bezier: Curve2f) {
        drawCurve(RenderConfig.SHAPE_PRIMARY, bezier, lineThick, 1000)

    }


    fun Renderer2D.drawCurvePoints(curve: Curve2f) {
        val steps = (curve.length() / baseStep).ceilToInt().clamp(0, limitLength)
        (0..steps).forEach {
            val p = curve.getPosition(it.toFloat() / steps)
            drawPoint(RenderConfig.SHAPE_PRIMARY, p)
        }

    }

    fun Renderer2D.drawCurveNormals(curve: Curve2f) {
        val steps = (curve.length() / baseStep).ceilToInt().clamp(0, limitLength)
        (0..steps).forEach {
            val t = it / steps.toFloat()
            val scale = 0.2f
            val position = curve.getPosition(t)
            val tangent = curve.getTangent(t) * scale
            val normal = curve.getNormal(t) * scale
            drawLine(RED, Line2f(position, position + tangent), lineThick)
            drawLine(GREEN, Line2f(position, position + normal), lineThick)
        }
    }

    fun place(root: Table) {
        val content = Table()
        val scroll = ScrollPane(content)
        root.add(scroll).growX()

        val skin = Assets.skin

        var rowTable: Table? = null

        val groupMap = mutableMapOf<String, MutableList<Pair<SelectorAction, TextButton>>>()

        actionManager.entries.forEach { entry ->

            val actor: Actor? = if (entry is SimpleAction) {
                createButton(entry.name) {
                    entry.action()
                }
            } else if (entry is SwitchAction) {
                createToggleButton(entry.name) {
                    entry.action(it)
                }
            } else if (entry is SelectorAction) {
                val button = createToggleButton(entry.name) {
                    entry.action(it)
                }
                groupMap.getOrPut(entry.group) { mutableListOf() } += Pair(entry, button)
                button
            } else if (entry is TitleEntry) {
                Label(entry.value, skin)
            } else {
                null
            }

            if (actor != null) {
                if (entry is SelectorAction) {
                    var rowTableNext = rowTable
                    if (rowTableNext == null) {
                        rowTableNext = Table()
                        rowTable = rowTableNext
                        content.addRow(rowTableNext).growX()
                    }
                    rowTableNext.add(actor).growX()
                } else {
                    if (rowTable != null) {
                        rowTable = null
                    }
                    content.addRow(actor)
                }
            }
        }

        groupMap.forEach { (_, group) ->
            val buttons = group.map { it.second }
            val selected = group.firstOrNull { it.first.isDefault }?.second ?: buttons.firstOrNull()
            ToggleGroup(buttons, selected)
        }
    }

}