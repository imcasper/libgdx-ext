package casperix.demo

import casperix.demo.city2.CityDemo2
import casperix.demo.city_region1.CityRegionDemo1
import casperix.demo.city_region1.IntercityRoadsDemo
import casperix.demo.city_region3.CityRegionDemo3
import casperix.demo.custom.*
import casperix.demo.custom.search.SpatialSearchDemo
import casperix.demo.impl.DemoLauncher
import casperix.math.vector.DivideGeometryDemo

object DemoCollection {
    val items: List<DemoLauncher> = listOf(
        DemoLauncher("Polygon") { PolygonDemo(it) },
        DemoLauncher("Intersection") { IntersectionDemo(it) },
        DemoLauncher("City 2") { CityDemo2(it) },
        DemoLauncher("City region 1") { CityRegionDemo1(it) },
        DemoLauncher("City region 3") { CityRegionDemo3(it) },
        DemoLauncher("Divide geometry") { DivideGeometryDemo(it) },
        DemoLauncher("Intercity roads") { IntercityRoadsDemo(it) },
        DemoLauncher("Spatial search") { SpatialSearchDemo(it) },
        DemoLauncher("Angle") { AngleDemo(it) },
        DemoLauncher("Arc") { ArcDemo(it) },
        DemoLauncher("Arc arrow") { ArcArrowDemo(it) },
        DemoLauncher("Line") { LineDemo(it) },
        DemoLauncher("Arc plus line") { ArcPlusLineDemo(it) },
        DemoLauncher("Bezier quadratic") { BezierQuadraticDemo(it) },
        DemoLauncher("Uniform bezier quadratic") { UniformBezierCubicDemo(it) },
        DemoLauncher("Bezier cubic") { BezierCubicDemo(it) },
        DemoLauncher("Curves grow") { CurveGrowDemo(it) },
        DemoLauncher("Angular") { AngularDemo(it) },
        DemoLauncher("Vector box graphic") { VectorBoxGraphicDemo(it) },
        DemoLauncher("Vector curve graphic") { VectorCurveGraphicDemo(it) },
        DemoLauncher("Normal map") { NormalMapDemo(it) },
        DemoLauncher("Flat map array") { FlatMapArrayDemo(it) },
        DemoLauncher("Normal map array") { NormalMapArrayDemo(it) },
        DemoLauncher("Line with line closest") { LineWithLineClosestDemo(it) },

        DemoLauncher("Old. CurveUnion") { CurveUnionDemo(it) },
        DemoLauncher("Old. Curve") { SplineCurveDemo(it) },
        DemoLauncher("Old. PointWithPolygonInt") { PointWithPolygonDemoInt(it) },
        DemoLauncher("Old. PointWithTriangleInt") { PointWithTriangleDemoInt(it) },
        DemoLauncher("Old. DestToLine") { DestToLineDemo(it) },
        DemoLauncher("Old. DestToSegment") { DestToSegmentDemo(it) },
        DemoLauncher("Old. PointWithTriangle") { PointWithTriangleDemo(it) },
    )
}