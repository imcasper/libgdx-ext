package casperix.demo.custom


import casperix.demo.impl.AbstractDemo
import casperix.math.color.Colors
import casperix.math.curve.float32.Bezier2f
import casperix.math.polar.float32.PolarCoordinateFloat
import casperix.math.random.nextFloat
import casperix.math.random.nextRadianFloat
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random

class SplineCurveDemo(val random: Random) : AbstractDemo() {

    val curve: Bezier2f
    val amount = random.nextInt(2, 6)
    val scale = 10f

    init {
        var last = Vector2f.ZERO
        val points = (0 until amount).map {
            val current = last

            val angle = random.nextRadianFloat()
            val range = random.nextFloat(0.2f, 1f) * scale
            last += PolarCoordinateFloat(range, angle).toDecart()

            current
        }

        curve = Bezier2f(points)
    }

    override fun update(customPoint: Vector2f) {

    }

    override fun getDetail(): String {
        return ""
    }

    override fun renderScene(renderer: Renderer2D) {
        repeat(amount - 1) { t ->
            val color = if (t % 2 == 0) Colors.RED else Colors.BLUE
            val tStart = t / (amount - 1).toFloat()
            val tFinish = (t + 1) / (amount - 1).toFloat()

//            val sub = curve.split(listOf(tStart, tFinish))[1]
//            renderer.drawCurve(color, sub, 0.02f)
        }

        repeat(21) {
            val p = curve.getPosition(it / 20f)
            renderer.drawPoint(Colors.WHITE, p, 0.05f)
        }
        curve.points.forEach {
            renderer.drawPoint(Colors.RED, it, 0.1f)
        }

    }
}