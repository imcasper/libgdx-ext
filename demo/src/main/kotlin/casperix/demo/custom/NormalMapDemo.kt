package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.QuadEditor
import casperix.demo.impl.shape_builder.TriangleEditor
import casperix.demo.misc.Assets
import casperix.math.geometry.PointAroundRay
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.random.nextQuad2f
import casperix.math.random.nextTriangle2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material
import kotlin.random.Random

@ExperimentalUnsignedTypes
class NormalMapDemo(val random: Random) : AbstractDemo() {
    val lightPosition = ControlPoint(Vector2f.ONE)

    val triangleEditor = TriangleEditor(pointManager, random.nextTriangle2f())

    val quadEditor = QuadEditor(pointManager, random.nextQuad2f())

    val material = Material(null, Assets.albedo, Assets.normals)

    init {
        pointManager.groups += listOf(lightPosition)
    }

    override fun update(customPoint: Vector2f) {
    }

    override fun getDetail(): String {
        val triangle = triangleEditor.triangle
        val pad = Geometry2Float.getPointAroundRay(triangle.v0, triangle.v1, triangle.v2, 0f)

        return "triangle CW:" + (pad == PointAroundRay.LEFT)
    }

    override fun renderScene(renderer: Renderer2D) {
        val triangle = triangleEditor.triangle
        renderer.environment = renderer.environment.copy(lightPosition = lightPosition.position.expand(0f))

        renderer.drawQuad(material, quadEditor.quad)
        renderer.drawTriangle(material, triangle)
    }

}


