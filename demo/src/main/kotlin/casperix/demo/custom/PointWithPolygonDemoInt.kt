package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.math.color.Colors
import casperix.math.intersection.int32.Intersection2Int
import casperix.math.random.nextPolygon2i
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.renderer.Renderer2D

import kotlin.random.Random

class PointWithPolygonDemoInt(val random: Random) : AbstractDemo() {

    val polygon = random.nextPolygon2i(-10..10, 2..5)
    var point = Vector2i.ZERO
    var hasIntersection = false
    var hasBorder = false


    override fun update(customPoint: Vector2f) {
        point = customPoint.roundToVector2i()

        hasIntersection = Intersection2Int.hasPointWithPolygon(point, polygon)
        hasBorder = Intersection2Int.pointWithPolygonEdge(point, polygon)
    }

    override fun getDetail(): String {


        return "Intersection: $hasIntersection\nBorder: $hasBorder"
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawPolygon(Colors.GREEN, polygon.convert { it.toVector2f() })

        val pointColor = if (hasBorder) {
            Colors.RED
        } else if (hasIntersection) {
            Colors.WHITE
        } else {
            Colors.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}