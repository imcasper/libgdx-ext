package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.angle.float32.DegreeFloat
import casperix.math.angle.float32.RadianFloat
import casperix.math.geometry.Line2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random


class AngleDemo(val random: Random) : AbstractDemo() {
    private val A = ControlPoint(Vector2f.X)
    private val B = ControlPoint(Vector2f.Y)

    private var degreeDirectedBetween = DegreeFloat.ZERO
    private var degreeBetween = DegreeFloat.ZERO
    private var degreeGreen = DegreeFloat.ZERO
    private var degreeRed = DegreeFloat.ZERO

    init {
        pointManager.groups += listOf(A, B)
    }

    override fun getArticles(): List<String> {
        return listOf(
            "red-green-directed: " + (degreeDirectedBetween).format(3),
            "red-green: " + (degreeBetween).format(3),
            "red-x: " + (degreeRed).format(3),
            "green-x: " + (degreeGreen).format(3),
        )
    }

    override fun update(customPoint: Vector2f) {
        degreeDirectedBetween = DegreeFloat.betweenDirectionsDirected(A.position, B.position)
        degreeBetween = DegreeFloat.betweenDirections(A.position, B.position)
        degreeGreen = DegreeFloat.byDirection(B.position)
        degreeRed = DegreeFloat.byDirection(A.position)
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawLine(SHAPE_SECONDARY, Line2f(Vector2f.ZERO, A.position))
        renderer.drawLine(SHAPE_SECONDARY, Line2f(Vector2f.ZERO, B.position))
    }

}

