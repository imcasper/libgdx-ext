package casperix.demo.custom.search

import casperix.math.axis_aligned.float32.Box2f
import casperix.math.curve.float32.Circle2f

interface Adapter {
    fun clear()
    fun add(element: Element)
    fun search(area: Box2f): Collection<Element>
    fun search(circle: Circle2f): Collection<Element>
}