package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.shape_builder.ArcPlusLineBuilder
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.angle.float32.DegreeFloat
import casperix.math.curve.float32.Arc2f
import casperix.math.curve.float32.CurveUnion2f
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.Line2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random


class ArcPlusLineDemo(val random: Random) : AbstractDemo() {
    private val arcBuilder = ArcPlusLineBuilder(random)
    private var arc = arcBuilder.build()


    init {
        pointManager.groups += arcBuilder.getPoints()
    }

    override fun getArticles(): List<String> {
        val curve = arc
        val curveInfo: List<String> = if (curve is Arc2f) {
            getArcInfo(curve)
        } else if (curve is LineCurve2f) {
            getLineInfo(curve)
        } else if (curve is CurveUnion2f) {
            val first = curve.first
            val second = curve.second

            if (first is Arc2f && second is LineCurve2f) {
                getArcInfo(first) + getLineInfo(second)

            } else if (first is LineCurve2f && second is Arc2f) {
                getLineInfo(first) + getArcInfo(second)
            } else {
                listOf()
            }
        } else {
            listOf()
        }

        val s1 = DegreeFloat.byDirection(arcBuilder.startTangent.position - arcBuilder.start.position)
        val t1 = DegreeFloat.byDirection(arc.getTangent(0f))

        val s2 = DegreeFloat.byDirection(arcBuilder.finishTangent.position - arcBuilder.finish.position)
        val t2 = DegreeFloat.byDirection(-arc.getTangent(1f))

        return (curveInfo + listOf(
            "start-angle (s): ${s1.format(5)}",
            "start-angle (t): ${t1.format(5)}",
            "finish-angle (s): ${s2.format(5)}",
            "finish-angle (t): ${t2.format(5)}",
        ))
    }

    fun getArcInfo(arc: Arc2f): List<String> {
        return listOf(
            "arc",
            "start-angle: " + arc.startAngle.toDegree(),
            "finish-angle: " + arc.finishAngle.toDegree(),
        )
    }

    fun getLineInfo(arc: LineCurve2f): List<String> {
        return listOf(
            "line",
            "start: " + arc.line.v0.toPrecision(2),
            "finish: " + arc.line.v1.toPrecision(2),
        )
    }

    override fun update(customPoint: Vector2f) {
        arc = arcBuilder.build()
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawCurve(arc)

        if (showHelpers) {
            renderer.drawCurvePoints(arc)
            renderer.drawCurveNormals(arc)

            renderer.drawLine(SHAPE_SECONDARY, Line2f(arcBuilder.start.position, arcBuilder.startTangent.position))
            renderer.drawLine(SHAPE_SECONDARY, Line2f(arcBuilder.finish.position, arcBuilder.finishTangent.position))
        }
    }


}

