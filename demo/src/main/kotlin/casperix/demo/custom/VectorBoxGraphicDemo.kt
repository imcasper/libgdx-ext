package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.misc.Assets
import casperix.math.angle.float32.RadianFloat
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.toQuad
import casperix.renderer.Renderer2D
import casperix.renderer.vector.VectorGraphic
import casperix.renderer.vector.builder.VectorBuilder
import kotlin.math.roundToInt
import kotlin.random.Random

@ExperimentalUnsignedTypes
class VectorBoxGraphicDemo(val random: Random) : AbstractDemo() {
    val O = ControlPoint(Vector2f(0f))
    val A = ControlPoint(Vector2f(1f, 0f))
    val Light = ControlPoint(Vector2f(2f, 2f))

    var amount = 0
    var angle = RadianFloat.ZERO
    var graphic = VectorGraphic.EMPTY

    init {
        pointManager.groups += listOf(O, A)
        pointManager.groups += listOf(Light)
    }

    override fun getDetail(): String {
        return listOf(
            "You can transform graphic",
            "And change light",
            "",
            "amount: $amount",
            "degree: ${angle.toDegree()}",
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
        val nextAmount = O.position.distTo(A.position).roundToInt()
        val nextAngle = RadianFloat.byDirection(A.position - O.position)

        if (amount != nextAmount) {
            rebuildGraphic(nextAmount)
        }

        amount = nextAmount
        angle = nextAngle
    }

    private fun rebuildGraphic(amount: Int) {
        graphic = VectorBuilder(hasPosition2 = true, hasTextureCoord = true, hasTangent = true).buildGraphic(Assets.small_lorry) {
            repeat(amount) {
                val vertices = Box2f.byDimension(Vector2f(it.toFloat(), 0f), Vector2f.ONE).toQuad().getVertices()
                val tanget = Vector2f.X
                addQuad {
                    val vertex = vertices[it]
                    setPosition2(vertex)
                    setTextureCoord(vertex)
                    setTangent(tanget)
                }
            }
        }
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.environment = renderer.environment.copy(lightPosition = Light.position.expand(1f))


        val delta = (A.position - O.position)

        val transform = Matrix3f.rotate(RadianFloat.byDirection(delta)) * Matrix3f.translate(O.position)
        renderer.drawGraphic(graphic, transform)
    }
}



