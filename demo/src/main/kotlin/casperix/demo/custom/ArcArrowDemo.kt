package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.shape_builder.ArcEditor
import casperix.demo.util.RenderConfig.SHAPE_PRIMARY
import casperix.math.geometry.builder.FixedSizeArrowMode
import casperix.math.geometry.builder.ProportionalArrowMode
import casperix.math.geometry.builder.UniformArrowMode
import casperix.math.random.nextArc2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material
import kotlin.random.Random


@ExperimentalUnsignedTypes
class ArcArrowDemo(val random: Random) : AbstractDemo() {
    private val arc1Editor = ArcEditor(pointManager, random.nextArc2f())
    private val arc2Editor = ArcEditor(pointManager, random.nextArc2f())
    private val arc3Editor = ArcEditor(pointManager, random.nextArc2f())


    override fun getDetail(): String {
        return ""
    }

    override fun update(customPoint: Vector2f) {

    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawArrow(Material(SHAPE_PRIMARY), arc1Editor.shape, 0.05f, UniformArrowMode(0.75f, 0.3f, 0.2f))
        renderer.drawArrow(Material(SHAPE_PRIMARY), arc2Editor.shape, 0.05f, ProportionalArrowMode(0.25f, 3f))
        renderer.drawArrow(Material(SHAPE_PRIMARY), arc3Editor.shape, 0.05f, FixedSizeArrowMode(0.5f, 0.2f))
    }

}

