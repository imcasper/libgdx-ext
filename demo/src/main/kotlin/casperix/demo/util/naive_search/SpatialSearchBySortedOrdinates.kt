package casperix.demo.util.naive_search

import casperix.math.axis_aligned.float32.Box2f
import java.util.*

class SpatialSearchBySortedOrdinates<Value> : SpatialSearch2D<Value> {

    private val xMap = TreeMap<Float, MutableSet<SpatialEntry2D<Value>>>()
    private val yMap = TreeMap<Float, MutableSet<SpatialEntry2D<Value>>>()

    override fun clear() {
        xMap.clear()
        yMap.clear()
    }

    override fun add(element: SpatialEntry2D<Value>) {
        addTo(xMap, element, element.position.x)
        addTo(yMap, element, element.position.y)

    }

    override fun remove(element: SpatialEntry2D<Value>) {
        removeFrom(xMap, element, element.position.x)
        removeFrom(yMap, element, element.position.y)

    }

    private fun addTo(
        map: TreeMap<Float, MutableSet<SpatialEntry2D<Value>>>,
        element: SpatialEntry2D<Value>,
        value: Float
    ) {
        map.getOrPut(value) { mutableSetOf() } += element
    }

    private fun removeFrom(
        map: TreeMap<Float, MutableSet<SpatialEntry2D<Value>>>,
        element: SpatialEntry2D<Value>,
        value: Float
    ) {
        map.getOrPut(value) { mutableSetOf() } -= element
    }

    override fun search(area: Box2f): Collection<SpatialEntry2D<Value>> {
        return intersect(
            xMap.subMap(area.min.x, area.max.x).values,
            yMap.subMap(area.min.y, area.max.y).values
        )
    }

    private fun intersect(
        a: MutableCollection<MutableSet<SpatialEntry2D<Value>>>,
        b: MutableCollection<MutableSet<SpatialEntry2D<Value>>>
    ): Collection<SpatialEntry2D<Value>> {
        return a.intersect(b.toSet()).flatten()
    }


}