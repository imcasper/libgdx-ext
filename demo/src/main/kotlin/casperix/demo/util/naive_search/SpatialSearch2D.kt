package casperix.demo.util.naive_search

import casperix.math.axis_aligned.float32.Box2f

interface SpatialSearch2D<Value> {
    fun clear()

    fun add(element: SpatialEntry2D<Value>)
    fun remove(element: SpatialEntry2D<Value>)
    fun search(area: Box2f): Collection<SpatialEntry2D<Value>>

    fun searchValues(area: Box2f): Collection<Value> {
        return search(area).map { it.value }
    }
}