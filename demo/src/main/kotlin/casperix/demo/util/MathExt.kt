package casperix.demo.util

import casperix.math.geometry.CustomPolygon
import casperix.math.geometry.Quad


fun <T : Any> Quad<T>.firstToEnd(): Quad<T> {
    return Quad(v1, v2, v3, v0)
}


fun <T : Any> CustomPolygon<T>.firstToEnd(): CustomPolygon<T> {
    if (points.size <= 1) return this


    val next = points.toMutableList()
    val first = next.removeFirst()
    return CustomPolygon(next + first)
}
