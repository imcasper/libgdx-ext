package casperix.demo.misc

import casperix.gdx.input.registerInput
import casperix.gdx.input.unregisterInput
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.misc.Disposable
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport

class GdxUIManager : Disposable {
    private val viewport = ScreenViewport()
    private val stage = Stage(viewport)
    private var main: Actor? = null

    init {
        registerInput(stage)
    }

    override fun dispose() {
        unregisterInput(stage)
        stage.dispose()
    }

    fun update() {
        stage.act(Gdx.graphics.deltaTime)
        stage.draw()

        if (stage.scrollFocus != null) {
            val mouse = Vector2i(Gdx.input.x, Gdx.input.y).toVector2f()
            if (hit(mouse) == null) {
                stage.scrollFocus = null
            }
        }
    }

    fun hit(screenPosition: Vector2f): Actor? {
        return stage.hit(screenPosition.x, Gdx.graphics.height - screenPosition.y, true)
    }

    fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
    }

    fun setActor(actor: Actor) {
        main?.let { unsetActor(it) }
        main = actor
        stage.addActor(actor)
    }

    fun addActor(actor: Actor) {
        stage.addActor(actor)
    }

    private fun unsetActor(actor: Actor) {
        if (actor == main) {
            main = null
            actor.remove()
        }
    }
}