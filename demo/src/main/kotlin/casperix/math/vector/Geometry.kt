package casperix.math.vector

import casperix.math.collection.getLooped
import casperix.math.geometry.CustomPolygon
import casperix.math.geometry.Line2f
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.float32.invert
import casperix.math.vector.float32.Vector2f


data class Polygon(val points: List<Vector2f>) {
    val edges = points.indices.map {
        val a = points.getLooped(it)
        val b = points.getLooped(it + 1)
        Edge(Line2f(a, b))
    }


    val median get() = points.reduce { a, b -> a + b } / points.size.toFloat()

    val shape: Polygon2f get() = CustomPolygon(points)
}

data class Edge(val connection: Line2f) {
    override fun equals(other: Any?): Boolean {
        val o = other as? Edge ?: return false
        return o.connection == connection || o.connection == connection.invert()
    }

    override fun hashCode(): Int {
        return connection.v0.hashCode() + connection.v1.hashCode()
    }
}

data class Geometry(val polygons: List<Polygon>) {
    val points: Set<Vector2f> get() = polygons.flatMap { it.points }.toSet()
    val edges: Set<Edge> get() = polygons.flatMap { it.edges }.toSet()
}


