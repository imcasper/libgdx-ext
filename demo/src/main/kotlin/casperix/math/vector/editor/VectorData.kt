package casperix.math.vector.editor

import casperix.math.geometry.*
import casperix.math.geometry.float32.isConvex
import casperix.math.vector.float32.Vector2f

class VectorData() {
    val polygons = mutableListOf<PolygonData>()
    val points get() = polygons.flatMap { it.points }
}

class PointData(var position: Vector2f) {
    fun distTo(other: PointData): Float {
        return position.distTo(other.position)
    }
}

class PolygonData(val polygon:CustomPolygon<PointData>) {
    constructor(points:List< PointData>) : this(CustomPolygon(points))
    constructor(vararg points: PointData) : this(points.toList())

    val points get() = polygon.points



    val isEmpty: Boolean get() = points.isEmpty()
    val isPoint: Boolean get() = points.size == 1
    val isLine: Boolean get() = points.size == 2
    val isTriangle: Boolean get() = points.size == 3
    val isQuad: Boolean get() = points.size == 4

    val triangle: Triangle<PointData>? get() = if (!isTriangle) null else Triangle(points[0], points[1], points[2])
    val quad: Quad<PointData>? get() = if (!isQuad) null else Quad(points[0], points[1], points[2], points[3])

    fun isConvex(): Boolean {
        return CustomPolygon(points.map { it.position }).isConvex()
    }

    companion object {

        fun from(shape: Polygon2f): PolygonData {
            return PolygonData(shape.getVertices().map { PointData(it) })

        }
    }
}


fun PolygonData.verticesReplaceLooped(): PolygonData {
    if (points.size <= 1) return this


    val next = points.toMutableList()
    val first = next.removeFirst()
    return PolygonData(next + first)
}

fun Line<PointData>.length():Float {
    return v0.position.distTo(v1.position)
}