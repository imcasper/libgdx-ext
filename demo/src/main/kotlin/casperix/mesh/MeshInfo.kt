package casperix.mesh

import casperix.math.mesh.float32.Mesh

object MeshInfo {
    fun get(mesh: Mesh): List<String> {
        return listOf(
            "regions: ${mesh.regions.size}",
            "edges: ${mesh.edges.size}",
            "points: ${mesh.points.size}",
        )
    }
}